let unidades = ["um", "dois", "tres", "quatro", "cinco", "seis", "sete", "oito", "nove", "dez", "onze", "doze",
                "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"]
let dezenas = [ "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"]
let dezenas2 = [ "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"]

let centena = ["cento", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos",
                 "oitocentos", "novecentos", "mil"]

const numbersToWords = (numero) => {
let string = ""

    if(numero < 20){

        string = unidades[numero - 1]

    }else if(numero < 100){

        let  index = (numero % 10) -1
        if(index !== -1){
            string = `${dezenas[Math.floor(numero/10) - 2]} e ${unidades[index]}`
        }else{
            string = dezenas[(numero/10) - 2]
        }

    }else if(numero === 100){
        string = "cem"

    }else if(numero <= 1000){

        let indexCentena = numero % 100
        let indexDezena = indexCentena % 10
            if(indexCentena === 0){
                string = centena[(numero/100) - 1]
            }else if(indexDezena === 0){
                string = `${centena[Math.floor(numero/100)- 1]} e ${dezenas2[(indexCentena/10) - 1]}`
            }else if(indexCentena <= 20){
                string = `${centena[Math.floor(numero/100) - 1]} e ${unidades[indexCentena -1]}`   
            }else if(numero < 1000){
                string = `${centena[Math.floor(numero/100) - 1]} e ${dezenas[Math.floor(indexCentena/10) - 2]} e ${unidades[indexDezena-1]}`
            }


    }
    return string

}


const printa = () => {
    for (let i=1; i<=1000; i++) { 
        let titulo = document.createElement('div')
        titulo.innerText = numbersToWords(i)
        document.querySelector("body").appendChild(titulo)
    }
}

printa()
